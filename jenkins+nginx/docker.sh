# docker.sh 文件内容~~~~~~~~~~~~~~~~~~~~~~
# 删除之前生成的映射文件
rm ./app -rf
# 删除垃圾文件
rm ./jenkins初始化密码.txt -f
# 停止 Docker Compose 中的所有容器，并删除相关的数据卷，包括项目的数据和缓存
docker-compose down -v
# 加载docker-compose
docker-compose up -d
# 休眠
sleep 10s
# 关闭docker-compose 拷贝配置文件
docker-compose down
# 将nginx配置文件拷贝到目标目录
rm ./app/nginx/nginx.conf -rf
cp ./config/nginx/nginx.conf ./app/nginx/nginx.conf -f
cp ./config/nginx/conf/ ./app/nginx/ -rf
# 将jenkins配置文件拷贝到目标目录
cp ./config/jenkins/* ./app/jenkins/ -r
# 重新加载docker-compose
docker-compose up -d
# 拷贝配置文件 可自行注释，前后对比再考虑是否需要执行该指令 
# 其中jenkins 要与你的docker-compose.yml 文件的jenkins容器名保持一致~
docker cp ./config/jenkins/config.xml jenkins:/var/jenkins_home/
# 提示
echo 等待jenkins初始化后再拷贝~ 也可以手动执行
# 休眠
sleep 60s
# 拷贝初始化密码
docker cp jenkins:/var/jenkins_home/secrets/initialAdminPassword ./jenkins_initKey.txt