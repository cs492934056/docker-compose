# 停止 Docker Compose 中的所有容器，并删除相关的数据卷，包括项目的数据和缓存
docker-compose down -v
# 执行docker-compose
docker-compose up -d