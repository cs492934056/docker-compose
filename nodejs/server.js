/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2023-02-16 16:22:25
 * @FilePath: \docker-compose\nodejs\server.js
 * @email: 492934056@qq.com
 */
'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});